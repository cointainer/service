<?php

namespace App\Http\Controllers;

use Closure;
use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class Controller extends BaseController
{
    private $cacheDuration;

    public function __contruct() {
        $this->cacheDuration = env('CACHE_DURATION');
    }

    public function getCache(string $key, Closure $callback) {
        return Cache::get($key, $callback);
    }

    public function setCache(string $key, $value) {
        Cache::put($key, $value, $this->cacheDuration);
    }

    public function forgetCache(string $key) {
        Cache::forget($key);
    }

    public function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => Auth::factory()->getTTL() * 60
        ], 200);
    }
}
