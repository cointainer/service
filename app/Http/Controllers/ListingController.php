<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Listing;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;

class ListingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->user = auth()->user();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cacheKey = '_listings_' . $this->user->username;

        $this->validate($request, [
            'per_page' => 'required_with:page|integer|min:0',
            'page' => 'min:0'
        ]);

        $listings = $this->getCache($cacheKey, function() {
            return Listing::where('user_id', $this->user->id)->with('coin.latestData')->get();
        });

        $this->setCache($cacheKey, $listings);

        return response()->json($listings);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cacheKey = '_listings_' . $this->user->username;

        $this->validate($request, [
            'coin_id'       => 'required|integer',
            'amount'        => 'required',
            'purchased_at'  => 'required',
        ]);

        try {
            Listing::create([
                'coin_id'       => Arr::get($request, 'coin_id'),
                'user_id'       => $this->user->id,
                'amount'        => Arr::get($request, 'amount'),
                'purchased_at'  => Arr::get($request, 'purchased_at'),
            ]);

            $this->forgetCache($cacheKey);

            return response()->json([
                'entity' => 'listing',
                'action' => 'create',
                'result' => 'success'
            ], 201);
        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return response()->json([
                'entity' => 'listing',
                'action' => 'create',
                'result' => 'failed'
            ], 409);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cacheKey = '_listings_' . $id;

        try {
            $listing = $this->getCache($cacheKey, function() use ($id) {
                return Listing::with('coin.data')->findOrFail($id);
            });
    
            $this->setCache($cacheKey, $listing);
    
            return response()->json($listing);
        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return response()->json([
                'entity' => 'listing',
                'action' => 'show',
                'result' => 'failed'
            ], 409);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cacheKey = '_listings_' . $id;

        $this->validate($request, [
            'coin_id'       => 'required|integer',
            'amount'        => 'required',
            'purchased_at'  => 'required',
        ]);

        try {
            $listing = Listing::findOrFail($id);

            $listing->coin_id = Arr::get($request, 'coin_id');
            $listing->user_id = $this->user->id;
            $listing->amount = Arr::get($request, 'amount');
            $listing->purchased_at = Arr::get($request, 'purchased_at');

            $listing->save();

            $this->forgetCache($cacheKey);
        
            return response()->json([
                'entity' => 'listing',
                'action' => 'update',
                'result' => 'success'
            ], 201);
        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return response()->json([
                'entity' => 'listing',
                'action' => 'update',
                'result' => 'failed'
            ], 409);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cacheKey = '_listings_' . $id;

        try {
            Listing::findOrFail($id)->delete();
            $this->forgetCache($cacheKey);
        
            return response()->json([
                'entity' => 'listing',
                'action' => 'delete',
                'result' => 'success'
            ], 201);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            
            return response()->json([
                'entity' => 'listing',
                'action' => 'delete',
                'result' => 'failed'
            ], 409);
        }
    }
}
