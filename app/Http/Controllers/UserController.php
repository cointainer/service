<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $perPage = $request->get('per_page', 25);
        $page = $request->get('page', 1);

        $this->validate($request, [
            'per_page' => 'required_with:page|integer|min:0',
            'page' => 'min:0'
        ]);

        $cacheKey = 'users_' . $page . '_' . $perPage;
        
        $users = $this->getCache($cacheKey, function() use($perPage) {
            return User::with('roles')->paginate($perPage);
        });

        $this->setCache($cacheKey, $users);

        return response()->json($users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|string|unique:users',
            'email'    => 'required|string|unique:users',
            'password' => 'required|confirmed',
        ]);

        try {
            User::create([
                'username' => $request->input('username'),
                'email' => $request->input('email'),
                'password' => app('hash')->make($request->input('password')),
            ]);

            return response()->json([
                'entity' => 'user',
                'action' => 'create',
                'result' => 'success'
            ], 201);
        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return response()->json([
                'entity' => 'user',
                'action' => 'create',
                'result' => 'failed'
            ], 409);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($username)
    {
        $cacheKey = 'user_' .  $username;

        $user = $this->getCache($cacheKey, function() use($username) {
            return User::where("username", $username)->with('roles.permissions')->get();
        });

        $this->setCache($cacheKey, $user);

        return response()->json($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        try {
            $user->delete();
            
            return response()->json([
                'entity' => 'user',
                'action' => 'delete',
                'result' => 'success'
            ], 201);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            
            return response()->json([
                'entity' => 'user',
                'action' => 'delete',
                'result' => 'failed'
            ], 409);
        }
    }
}
