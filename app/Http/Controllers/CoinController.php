<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\Coin;
use App\Models\CoinData;

class CoinController extends Controller
{
    private $apiKey;
    private $limit;
    private $convert;

    public function __construct()
    {
        $this->middleware('auth:api');
        $this->apiKey = env('CMC_API_KEY');
        $this->limit = env('CMC_API_LIMIT');
        $this->convert = env('CMC_API_CURRENCY_CONVERT');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $perPage = $request->get('per_page', 25);
        $page = $request->get('page', 1);

        $this->validate($request, [
            'per_page' => 'required_with:page|integer|min:0',
            'page' => 'min:0'
        ]);
        
        $coins = $this->getCache('coins_' . $page . '_' . $perPage, function() use($perPage) {
            return Coin::with('data')->paginate($perPage);
        });

        $this->setCache('coins_' . $page . '_' . $perPage, $coins);

        return response()->json($coins);
    }

    /**
     * Request new data from CoinMarketCap API.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateCoinMarketCapData()
    {
        $latestData = Coin::first() !== null ? Coin::first()->latestData : null;

        if($latestData && strtotime($latestData->updated_at) >= strtotime('-4 hours')) {
            return response()->json('No need to refresh!');
        };
        
        $coins = $this->_getCoinMarketCapData();
        
        if (empty($coins->data)) return;
        
        foreach ($coins->data as $c) {
            $coin = $this->getCache($c->symbol, function() use($c) {
                return Coin::with('data')->where("symbol", $c->symbol)->first();
            });
            
            if($coin == null) {
                $coin = Coin::create([
                    "coin_id" => $c->id,
                    "name" => $c->name,
                    "symbol" => $c->symbol,
                    "slug" => $c->slug,
                ]);
            }
                
            $this->setCache($c->symbol, $coin);

            CoinData::create([
                "name" => $c->name,
                "price" => $c->quote->USD->price,
                "percent_change_1h" => $c->quote->USD->percent_change_1h,
                "percent_change_24h" => $c->quote->USD->percent_change_24h,
                "percent_change_7d" => $c->quote->USD->percent_change_7d,
                "coin_id" => $coin->id,
            ]);
        }

        return response()->json('Success');
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $symbol
     * @return \Illuminate\Http\Response
     */
    public function show($symbol)
    {
        $coin = $this->getCache($symbol, function() use($symbol) {
            return Coin::where("symbol", $symbol)->with('data')->get();
        });

        $this->setCache($symbol, $coin);

        return response()->json($coin);
    }

    private function _getCoinMarketCapData() {
        $client = new Client();

        $response = $client->request('GET', "https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest", [
            'query' => [
                'CMC_PRO_API_KEY' =>$this->apiKey,
                'convert' => $this->convert,
                'limit' => $this->limit,
            ]
        ]);

        $coins = $response->getBody()->getContents();
        return json_decode($coins);
    }
}
