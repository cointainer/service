<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

use App\Models\CoinData;

class Coin extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'coin_id', 'name', 'symbol', 'slug',
    ];

    public function data(): HasMany
    {
        return $this->hasMany(CoinData::class);
    }

    public function latestData(): HasOne
    {
        return $this->hasOne(CoinData::class)->latest();
    }
}
