<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoinDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coin_data', function (Blueprint $table) {
            $table->id();
            $table->float('price', 17, 2);
            $table->float('percent_change_1h', 9, 4)->nullable();
            $table->float('percent_change_24h', 9, 4)->nullable();
            $table->float('percent_change_7d', 9, 4)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coin_data');
    }
}
