<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Role::where('slug', 'administrator')->first();
        $user = Role::where('slug', 'user')->first();

        User::create([
            'username' => 'admin',
            'email' => 'admin@cointainer.app',
            'password' => Hash::make('admin')
        ])->roles()->attach($admin);

        User::create([
            'username' => 'user',
            'email' => 'user@cointainer.app',
            'password' => Hash::make('user')
        ])->roles()->attach($user);
    }
}
