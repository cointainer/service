<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\Permission;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $config = config('auth.role_structure');
        $permissionMap = collect(config('auth.permissions_map'));

        foreach($config as $key => $modules) {
            $role = Role::create([
                'name' => ucwords(str_replace("_", " ", $key)),
                'slug' => $key,
            ]);

            $this->command->info('Creating Role '. strtoupper($key));

            foreach ($modules as $module => $value) {
                $permissions = explode(',', $value);

                foreach ($permissions as $p => $perm) {
                    $permissionValue = $permissionMap->get($perm);

                    $permission = Permission::firstOrCreate([
                        'name' => ucfirst($permissionValue) . ' ' . ucfirst($module),
                        'slug' => $permissionValue . '-' . $module,
                        'description' => ucfirst($permissionValue) . ' ' . ucfirst($module),
                    ]);

                    $this->command->info('Creating Permission to '.$permissionValue.' for '. $module);

                    $role->permissions()->attach($permission);
                }
            }
        }
    }
}
