<?php

namespace Database\Seeders;

use App\Models\Coin;
use App\Models\CoinData;
use App\Models\Listing;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            RoleSeeder::class,
            UserSeeder::class
        ]);

        for ($i = 0; $i <= 30; $i++) {
            Listing::factory()->for(Coin::factory()->hasData(10))->create(['user_id' => 1]);
        }
    }
}
