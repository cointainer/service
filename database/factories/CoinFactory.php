<?php

namespace Database\Factories;

use App\Models\Coin;
use Illuminate\Database\Eloquent\Factories\Factory;

class CoinFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Coin::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'coin_id' => $this->faker->randomNumber(5, false),
            'name' => $this->faker->word(),
            'symbol' => $this->faker->regexify('[A-Z]{3}'),
            'slug' => $this->faker->word(),
        ];
    }
}
