<?php

namespace Database\Factories;

use App\Models\Coin;
use App\Models\CoinData;
use Illuminate\Database\Eloquent\Factories\Factory;

class CoinDataFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CoinData::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'price' => $this->faker->randomFloat(2, 0, 5000),
            'percent_change_1h' => $this->faker->randomFloat(2, -20, 20),
            'percent_change_24h' => $this->faker->randomFloat(2, -20, 20),
            'percent_change_7d' => $this->faker->randomFloat(2, -20, 20),
        ];
    }
}
